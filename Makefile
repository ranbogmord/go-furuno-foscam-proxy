build_linux_amd64:
	GOOS=linux GOARCH=amd64 go build -o bin/furuno-foscam-proxy-linux-amd64 .

build_linux_arm64:
	GOOS=linux GOARCH=arm64 go build -o bin/furuno-foscam-proxy-linux-arm64 .

build_linux_arm:
	GOOS=linux GOARCH=arm go build -o bin/furuno-foscam-proxy-linux-arm .

build_windows_x64:
	GOOS=windows GOARCH=amd64 go build -o bin/furuno-foscam-proxy-win64.exe .

clean:
	if [ -d bin ]; then rm -r bin; fi

build: clean build_linux_amd64 build_linux_arm64 build_linux_arm build_windows_x64
