# Furuno Foscam Proxy (Go version)
Rewrite / Optimized version of [Furuno Foscam Proxy](https://gitlab.com/ranbogmord/furunofoscamproxy) in go.

## Configuration
Configuration can be passed either using a config file or on the command line.

### Config file
```yaml
camera_ip: "" # IP of Furuno camera
camera_username: "" # Username on Furuno camera
camera_password: "" # Password on Furuno camera
bind_address: "" # Which address to bind this application to (default: :80)
```

### Command Line
```bash
furuno-foscam-proxy -camera-ip=n.n.n.n -camera-username=admin -camera-password=password -bind-address=0.0.0.0:80
```
## Building
A binary can be built using the included `Makefile`.
```bash
make build
```

See the Makefile for available targets.
