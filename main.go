package main

import (
	"embed"
	"flag"
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

//go:embed default.jpg
var f embed.FS
var currentImg []byte

type Config struct {
	CameraIP       string `yaml:"camera_ip"`
	CameraUsername string `yaml:"camera_username"`
	CameraPassword string `yaml:"camera_password"`
	BindAddress    string `yaml:"bind_address"`
}

var config Config

func main() {
	flag.StringVar(&config.CameraIP, "camera-ip", "", "Foscam IP")
	flag.StringVar(&config.CameraUsername, "camera-username", "", "Foscam username")
	flag.StringVar(&config.CameraPassword, "camera-password", "", "Foscam password")
	flag.StringVar(&config.BindAddress, "bind-address", ":80", "HTTP server bind address")

	flag.Parse()

	if config.CameraIP == "" {
		_, err := os.Stat("config.yaml")
		if err == nil {
			conf, err := ioutil.ReadFile("config.yaml")
			if err != nil {
				log.Printf("Unable to read config file: %v", err)
			} else {
				_ = yaml.Unmarshal(conf, &config)

				log.Printf("Using config file: %v", config)
			}
		}
	}

	ticker := time.NewTicker(time.Millisecond * 250)

	go func() {
		client := http.Client{}

		for {
			<-ticker.C
			res, err := client.Get(fmt.Sprintf(
				"http://%s/cgi-bin/CGIProxy.fcgi?cmd=snapPicture2&usr=%s&pwd=%s",
				config.CameraIP,
				config.CameraUsername,
				config.CameraPassword,
			))

			if err != nil {
				log.Printf("Failed to load image: %v", err)
			} else {
				var tempBuf []byte
				readBytes, err := res.Body.Read(tempBuf)
				res.Body.Close()
				if err == nil && readBytes > 0 {
					currentImg = tempBuf
				}
			}
		}
	}()

	currentImg, _ = f.ReadFile("default.jpg")
	log.Printf("Loaded default image %d", len(currentImg))

	http.HandleFunc("/", MuxHandler)
	log.Fatal(http.ListenAndServe(config.BindAddress, nil))
}

func MuxHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("GET %s", r.RequestURI)

	if r.URL.Path == "/axis-cgi/jpg/image.cgi" {
		ImageHandler(w, r)
	} else if r.URL.Path == "/axis-cgi/com/ptz.cgi" {
		CommandHandler(w, r)
	}
}

func ImageHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "image/jpeg")
	w.Write(currentImg)
}

func CommandHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Query().Has("rpan") {
		rpan := r.URL.Query().Get("rpan")

		if rpan == "3" {
			// ptzMoveRight, ptzStopRun
			SendCameraCommand("ptzMoveRight", "ptzStopRun")
		} else if rpan == "-3" {
			// ptzMoveLeft, ptzStopRun
			SendCameraCommand("ptzMoveLeft", "ptzStopRun")
		}
	} else if r.URL.Query().Has("rtilt") {
		rtilt := r.URL.Query().Get("rtilt")

		if rtilt == "3" {
			// ptzMoveUp, ptzStopRun
			SendCameraCommand("ptzMoveUp", "ptzStopRun")
		} else if rtilt == "-3" {
			// ptzMoveDown, ptzStopRun
			SendCameraCommand("ptzMoveDown", "ptzStopRun")
		}
	} else if r.URL.Query().Has("rzoom") {
		rzoom := r.URL.Query().Get("rzoom")

		if rzoom == "100" {
			// zoomIn, zoomStop
			SendCameraCommand("zoomIn", "zoomStop")
		} else if rzoom == "-100" {
			// zoomOut, zoomStop
			SendCameraCommand("zoomOut", "zoomStop")
		}
	} else if r.URL.Query().Has("move") {
		move := r.URL.Query().Get("move")

		if move == "home" {
			// ptzReset
			SendCameraCommand("ptzReset", "")
		}
	}
}

func SendCameraCommand(cmd, stopCmd string) {
	client := http.Client{}

	_, err := client.Get(fmt.Sprintf(
		"http://%s/cgi-bin/CGIProxy.fcgi?cmd=%s&usr=%s&pwd=%s",
		config.CameraIP,
		cmd,
		config.CameraUsername,
		config.CameraPassword,
	))

	if err != nil {
		log.Printf("Failed to send camera command: %v", err)
		return
	}

	if stopCmd != "" {
		time.Sleep(time.Millisecond * 250)
		_, err = client.Get(fmt.Sprintf(
			"http://%s/cgi-bin/CGIProxy.fcgi?cmd=%s&usr=%s&pwd=%s",
			config.CameraIP,
			stopCmd,
			config.CameraUsername,
			config.CameraPassword,
		))

		if err != nil {
			log.Printf("Failed to send camera command: %v", err)
		}
	}
}
